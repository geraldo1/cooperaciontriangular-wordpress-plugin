<?php
/*
Plugin Name: Cooptri
Description: Custom post types for project Cooptri
Author: Gerald Kogler
Author URI: https://geraldo.github.io
Text Domain: cooptri
*/

/**
 * load textdomain
 */
add_action( 'init', 'cooptri_load_textdomain' );
function cooptri_load_textdomain() {
	load_plugin_textdomain('cooptri', false, dirname(plugin_basename(__FILE__)));
}

/**
 * limit searches to post_type
 */
add_filter('pre_get_posts', 'cooptri_filter_search_publications_autores');
function cooptri_filter_search_publications_autores($query) {

	//trigger_error(json_encode($query), E_USER_WARNING);

    if( $query->is_search ) 
    	//$query->set('post_type', array('publication', 'publication_autor'));
    	$query->set('post_type', array('post', 'publication'));

    return $query;
}

/**
 * Join posts and postmeta tables
 *
 * https://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
/*function cooptri_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cooptri_search_join' );*/

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
/*function cooptri_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cooptri_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
/*function cooptri_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cooptri_search_distinct' );*/

/**
 * Limit the main query search results to 25.
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/post_limits
 */
/*function cooptri_filter_main_search_post_limits( $limit, $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
		return 'LIMIT 0, 100';
	}

	return $limit;
}
add_filter( 'post_limits', 'cooptri_filter_main_search_post_limits', 10, 2 );*/

function cooptri_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'cooptri_login_logo_url' );

/**
 * Add custom post types to 'At a Glance' widget
 */
function cooptri_add_custom_post_counts() {
   $post_types = array('publication');  // array of custom post types to add to 'At A Glance' widget
   foreach ($post_types as $pt) :
      $pt_info = get_post_type_object($pt); // get a specific CPT's details
      $num_posts = wp_count_posts($pt); // retrieve number of posts associated with this CPT
      $num = number_format_i18n($num_posts->publish); // number of published posts for this CPT
      $text = _n( $pt_info->labels->singular_name, $pt_info->labels->name, intval($num_posts->publish) ); // singular/plural text label for CPT
      echo '<li class="page-count '.$pt_info->name.'-count"><a href="edit.php?post_type='.$pt.'">'.$num.' '.$text.'</a></li>';
   endforeach;
}
add_action('dashboard_glance_items', 'cooptri_add_custom_post_counts');

/**
 * define custom post type PUBLICATION
 */
function cooptri_register_publication() {

	$labels = array(
		"name" => __( "Publications", "cooptri" ),
		"singular_name" => __( "Publication", "cooptri" ),
	);

    register_taxonomy( 'author', 'publication', array(
        'label'        => 'Author',
        'rewrite'      => array( 'slug' => 'author' ),
        'capabilities' => array(
            'assign_terms' => 'edit_publications',
            'edit_terms' => 'publish_publications',
            'manage_terms' => 'publish_publications',
            'delete_terms' => 'publish_publications'
        )
    ) );

    register_taxonomy( 'entity', 'publication', array(
        'label'        => 'Entity',
        'rewrite'      => array( 'slug' => 'entity' ),
        'capabilities' => array(
            'assign_terms' => 'edit_publications',
            'edit_terms' => 'publish_publications',
            'manage_terms' => 'publish_publications',
            'delete_terms' => 'publish_publications'
        )
    ) );

	register_taxonomy( 'publication_category', 'publication', array(
		'label'		   => 'Type of document',
        'rewrite'      => array( 'slug' => 'publication_category' ),
        'capabilities' => array(
			'assign_terms' => 'edit_publications',
			'edit_terms' => 'publish_publications'
		)
    ) );

    //wp_insert_term('Estudio', 'publication_category');
    //wp_insert_term('Informe', 'publication_category');
    //wp_insert_term('Libro', 'publication_category');
    //wp_insert_term('Capítulo Libro', 'publication_category');
    //wp_insert_term('Artículo', 'publication_category');
    //wp_insert_term('Declaración / Documento de posición', 'publication_category');
    //wp_insert_term('Resolución organismo oficial', 'publication_category');
    //wp_insert_term('Otras publicaciones', 'publication_category');

    register_taxonomy( 'publication_tag', 'publication', array(
        'label'        => 'Tags',
        'rewrite'      => array( 'slug' => 'publication_tag' ),
        'capabilities' => array(
            'assign_terms' => 'edit_publications',
            'edit_terms' => 'publish_publications',
            'manage_terms' => 'publish_publications',
            'delete_terms' => 'publish_publications'
        )
    ) );

	$args = array(
		"label" => __( "Publications", "cooptri" ),
		"labels" => $labels,
		"description" => "cooptri publication",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "publications",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "publication",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "publication", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-book",
		"supports" => array( "title", "editor", "excerpt", "thumbnail", "revisions" ),
		"taxonomies" => array( "author", "entity", "publication_category", "publication_tag" ),
	);

	register_post_type( "publication", $args );
}

add_action( 'init', 'cooptri_register_publication' );

/*
	ACF field groups
*/
if( function_exists('acf_add_local_field_group') ) {

    acf_add_local_field_group(array(
        'key' => 'group_6020f428ba29a',
        'title' => 'Publicaction',
        'fields' => array(
            array(
                'key' => 'field_6020f4324f365',
                'label' => 'Date of publicaction',
                'name' => 'date_of_publicaction',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'm/Y',
                'return_format' => 'Ymd',
                'first_day' => 1,
            ),
            array(
                'key' => 'field_6020f47d4f366',
                'label' => 'Author',
                'name' => 'author',
                'type' => 'taxonomy',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => 'author',
                'field_type' => 'multi_select',
                'allow_null' => 0,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array(
                'key' => 'field_6020f47d4f477',
                'label' => 'Entity',
                'name' => 'entity',
                'type' => 'taxonomy',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => 'entity',
                'field_type' => 'multi_select',
                'allow_null' => 0,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array(
                'key' => 'field_6020f6fc49488',
                'label' => 'Type of document',
                'name' => 'type_of_document',
                'type' => 'taxonomy',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => 'publication_category',
                'field_type' => 'radio',
                'add_term' => 0,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
                'allow_null' => 0,
            ),
            array(
                'key' => 'field_6020f75449489',
                'label' => 'Tags',
                'name' => 'tags',
                'type' => 'taxonomy',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => 'publication_tag',
                'field_type' => 'multi_select',
                'allow_null' => 0,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array(
                'key' => 'field_602114aaac4c7',
                'label' => 'Own publication',
                'name' => 'own_publication',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 0,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
            array(
                'key' => 'field_60508a9127619',
                'label' => 'Archivo o URL externa',
                'name' => 'archivo_o_url_externa',
                'type' => 'radio',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'Archivo' => 'Archivo',
                    'URL' => 'URL',
                ),
                'allow_null' => 0,
                'other_choice' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
                'return_format' => 'value',
                'save_other_choice' => 0,
            ),
            array(
                'key' => 'field_602114e6ac5b7',
                'label' => 'Archivo',
                'name' => 'archivo',
                'type' => 'file',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_60508a9127619',
                            'operator' => '==',
                            'value' => 'Archivo',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_60508a4c05065',
                'label' => 'URL',
                'name' => 'url',
                'type' => 'url',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_60508a9127619',
                            'operator' => '==',
                            'value' => 'URL',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
            array(
                'key' => 'field_60508c30f9edb',
                'label' => 'Título URL',
                'name' => 'titulo_url',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_60508a9127619',
                            'operator' => '==',
                            'value' => 'URL',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'publication',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
}

/**
 * add capabilities for type PUBLICATION
 */
function cooptri_add_cap_upload() {

    // admin
    $role = get_role( 'administrator' );

    $role->add_cap( 'edit_publications' ); 
    $role->add_cap( 'edit_published_publications' ); 
    $role->add_cap( 'edit_private_publications' ); 
    $role->add_cap( 'edit_others_publications' ); 
    $role->add_cap( 'publish_publications' ); 
    $role->add_cap( 'read_private_publications' ); 
    $role->add_cap( 'delete_private_publications' ); 
    $role->add_cap( 'delete_publications' ); 
    $role->add_cap( 'delete_published_publications' ); 
    $role->add_cap( 'delete_others_publications' );

    // editor
    $role = get_role( 'editor' );

    $role->add_cap( 'edit_publications' ); 
    $role->add_cap( 'edit_published_publications' ); 
    $role->add_cap( 'edit_private_publications' ); 
    $role->add_cap( 'edit_others_publications' ); 
    $role->add_cap( 'publish_publications' ); 
    $role->add_cap( 'read_private_publications' ); 
    $role->add_cap( 'delete_publications' ); 
    $role->add_cap( 'delete_private_publications' ); 
    $role->add_cap( 'delete_published_publications' ); 
    $role->add_cap( 'delete_others_publications' );
}
add_action( 'init', 'cooptri_add_cap_upload' );

function cooptri_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cooptri_mime_types');


/**
 * limit searches to post_type
 */
/*add_filter('pre_get_posts', 'cooptri_filter_search_obras_autores');
function cooptri_filter_search_obras_autores($query) {

    //trigger_error(json_encode($query), E_USER_WARNING);

    if( $query->is_search ) 
        //$query->set('post_type', array('obra', 'obra_autor'));
        $query->set('post_type', array('obra'));

    return $query;
}*/

/**
 * Join posts and postmeta tables
 *
 * https://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
/*function cooptri_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cooptri_search_join' );*/

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
/*function cooptri_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cooptri_search_where' );*/

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
/*function cooptri_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cooptri_search_distinct' );*/

/**
 * Limit the main query search results to 25.
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/post_limits
 */
function cooptri_filter_main_search_post_limits( $limit, $query ) {

    if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
        return 'LIMIT 0, 25';
    }

    return $limit;
}
add_filter( 'post_limits', 'cooptri_filter_main_search_post_limits', 10, 2 );

?>